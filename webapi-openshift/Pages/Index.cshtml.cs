﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace webapi_openshift.Pages
{
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
            ViewData["server"] = Dns.GetHostName();

        }
    }
}
